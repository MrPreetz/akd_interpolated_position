// *****************************************************************
// DEFINES - Pool for all global constant 
// 
// DISCLAIMER:
// [MIT](https://choosealicense.com/licenses/mit/)
// Copyright (c) [2021] [Oxni GmbH]
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Version	Date		Who			What
// *****************************************************************
// V 1.002	04.10.21	MRupf Oxni	Adaption for Interpolated Position
// V 1.001	28.07.21	MRupf Oxni	Adaption for Profile Velocity and S6
// V 0.002	03.06.20	MRupf Oxni	AKD_MT added
// V 0.001	25.05.20	MRupf Oxni	First version
// *****************************************************************

#pragma once
#include <stdint.h>

// SW Version
static const uint64_t SW_VERSION_DEFINES = 0x1002;

// Constants Real-Time
const uint64_t REALTIME_PERIOD = 2000;		// Realtime sampling period [usec]
const uint64_t SYNC_CYCLES = 1;				// EtherCat Cycle time 2ms = 2 * 1000usec
const double CYCLE_TIME_IN_MS = 0.002;

// Constant DS402
const uint16_t BIT_MASK_DRIVE_FAULT = 0x0008;		// Bit 3
const uint16_t BIT_MASK_DRIVE_WARNING = 0x0080;		// Bit 7
const uint16_t BIT_MASK_DRIVE_STATUS = 0x007F;		// Bit 0-6
const uint16_t BIT_MASK_DRIVE_CMD = 0x000F;			// Bit 0-3
const uint16_t BIT_MASK_DRIVE_RESET = 0x0080;		// Bit 7
const uint16_t BIT_MASK_DRIVE_STOP = 0x0100;		// Bit 8

// Constants AKD
const double POS_SCALE_DRIVE_AKD = 1048576;			// 2^FB1.PSCALE
const uint16_t BIT_MASK_DRIVE_STO_AKD = 0x0100;		// Bit 8

// Profile Position Mode, Home Mode
const uint16_t BIT_MASK_DRIVE_MAN1_AKD = 0x1000;	// Bit 12	setpoint acknowledged,	Home done
const uint16_t BIT_MASK_DRIVE_MAN2_AKD = 0x2000;	// Bit 13	following error,		Home error
const uint32_t JOG_TIMEOUT_COUNT = 0;				// Number of realtime cycles till jog request will stop itself, to disable set 0

// Constants S6
const double POS_SCALE_DRIVE_S6 = 1048576.0;		// co03
const double VEL_SCALE_DRIVE_S6 = 16.0;				// co02
const double CUR_SCALE_DRIVE_S6 = 100.0;			// ru10 fix resolution of x.00 Arms
const int32_t VEL_CMD_SCALE_DRIVE_S6 = 8192;		// vl.21 fix resolution of 1/8192rpm
const uint16_t BIT_MASK_DRIVE_STO_S6 = 0xC000;		// Bit 14+15

 
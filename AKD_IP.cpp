#include "AKD_IP.h"
#include "DEFINES.h"

// Constructor
AkdInterpolatedPosition::AkdInterpolatedPosition(double posScaling, float driveCurrent)
	:txMap(nullptr)
	, rxMap(nullptr)
	, _posScaling(posScaling)
	, _driveIcont(driveCurrent)
{
}

// Return drive fault
bool AkdInterpolatedPosition::getDriveFault() {
	return txMap != nullptr ? txMap->statusWord & BIT_MASK_DRIVE_FAULT : false;
};

// Return drive warning
bool AkdInterpolatedPosition::getDriveWarning() {
	return txMap != nullptr ? txMap->statusWord & BIT_MASK_DRIVE_WARNING : false;
};

// Return home status
bool AkdInterpolatedPosition::getHomeStatus() {
	return _homeDone;
};

// Return error status
bool AkdInterpolatedPosition::getErrorStatus() {
	return _error;
};

// Return enable status
bool AkdInterpolatedPosition::getEnableStatus() {
	return _isEnabled;
};

// Return status as bit combined int
uint32_t AkdInterpolatedPosition::getDriveStatus() {
	uint32_t temp = (int)_isEnabled * 2;	// Bit 1 Enabled
	temp |= _homeDone * 8; // Bit 3 Home done
	temp |= _error * 32; // Bit 5 Error

	// Only read the bus bits if the pointer is valid
	if (txMap != nullptr) {
		temp |= (bool)(txMap->statusWord & 1024) * 4; // Bit 2 In-Pos (Bit 10 in 6041)
		temp |= (bool)(txMap->statusWord & 8) * 16; // Bit 4 Fault (Bit 3 in 6041)
		temp |= (bool)(txMap->statusWord & 128) * 64; // Bit 6 Warning (Bit 7 in 6041)
		temp |= (bool)(txMap->statusWord & 256) * 128; // Bit 7 STO (Bit 8 in 6041)
	}
	return temp;
}

// Get Actual Position as float in User Units
double AkdInterpolatedPosition::getActPosUU() {
	return txMap != nullptr ? (double)((_internalPos + (_internalPosOverflow * ULONG_MAX)) / POS_SCALE_DRIVE_AKD) * _posScaling : 0.0;
}

// Get Actual Speed in RPM
double AkdInterpolatedPosition::getActVel() {
	return txMap != nullptr ? (double)txMap->actVel : 0.0; // No need for scaling with FW 1-19 ?? / 1000.0;
}

// Get Actual Current in Arms
double AkdInterpolatedPosition::getActCurrent() {
	return txMap != nullptr ? ((double)txMap->actCurrent) * _driveIcont / 1000.0f : 0.0;
}

// Get Actual Status of the digital inputs (bit 16 - 22)
uint32_t AkdInterpolatedPosition::getDigInStat() {
	uint32_t temp = 0;
	if (txMap != nullptr) {
		temp = (txMap->digitalInput >> 15) & 255;	// get all input (bit 16-22)
		temp |= _digitalOutput;							// Bitwise or the digital output (bit 16-17)
	}
	return temp;
}

//Realtime task this routine has to be called in each cycle
void AkdInterpolatedPosition::realTimeTask(const TX_MAP_AKD_v2* Tx_Map, RX_MAP_AKD_v2* Rx_Map) {
	txMap = Tx_Map;
	rxMap = Rx_Map;
	uint16_t cmdWord = 0;
	uint16_t statusWord = 0;

	if (txMap != nullptr) {
		// Read Position and include overflow (upscaling to 64-Bit from 32-Bit)
		if (_internalPos != 0) {
			if (((int64_t)_internalPos - txMap->actPos) > (1073741824)) {
				_internalPosOverflow++;	// + Position overflow
			}
			else if ((int64_t)(_internalPos - txMap->actPos) < (-1073741824)) {
				_internalPosOverflow--;	// - Position overflow
			}
		}
		_internalPos = txMap->actPos;

		statusWord = (txMap->statusWord & BIT_MASK_DRIVE_STATUS);
		_actVel = txMap->actVel / 60 * CYCLE_TIME_IN_MS;	// Velocity per cycle

		if (_enable) {
			switch (statusWord)
			{
				// Switch on disabled
			case 0x50:
			case 0x70:
				cmdWord = 0x06;
				break;
				// Ready to switch on
			case 0x31:
				cmdWord = 0x07;
				break;
				// Switched On
			case 0x33:
				cmdWord = 0x0F;
				break;
				// Operation enabled
			case 0x37:
				_isEnabled = true;

				// Call motion coordinator if not homing
				if (!_doHome) { //realTimeMotionCoordinator();
					// Calc Jerk table
					if (_stop) {
						_profilGen.calcJog(_vel, 0.0, _cmdDec);	// Calculate move profile
						_CycleIdx = 0;	// Start Cycle counting at 0
						_ProfilIdx = 0;	// Start Profile new
						_moveActive = true;
					}
					else {
						if (_jog) {
							_profilGen.calcJog(_vel, _cmdVel, _cmdAcc);	// Calculate move profile
							_CycleIdx = 0;	// Start Cycle counting at 0
							_ProfilIdx = 0;	// Start Profile new
							_moveActive = true;
						}
						else {
							if (_move) {
								_profilGen.calcMoveRel(_vel, _cmdPos, _cmdVel, _cmdAcc, _cmdDec);	// Calculate move profile
								_CycleIdx = 0;	// Start Cycle counting at 0
								_ProfilIdx = 0;	// Start Profile new
								_moveActive = true;
							}
							else {
								if (_moveAbs) {
									// Calculate move distance
									_cmdPos = _cmdPos - _pos;
									_profilGen.calcMoveRel(_vel, _cmdPos , _cmdVel, _cmdAcc, _cmdDec);	// Calculate move profile
									_CycleIdx = 0;	// Start Cycle counting at 0
									_ProfilIdx = 0;	// Start Profile new
									_moveActive = true;
								}
							}
						}
					}
					// Reset profile generator bits, to avoid multiple calls
					_stop = false;
					_jog = false;
					_move = false;
					_moveAbs = false;

					// Start motion
					if (_ProfilIdx == 0) {
						_cmdPos3 = (double)_internalPos + (_internalPosOverflow * ULONG_MAX);
					}
					if (_profilGen._jerkTable[_ProfilIdx].idx == _CycleIdx) {
						_acc = _profilGen._jerkTable[_ProfilIdx].jerk;
						++_ProfilIdx;
					}
				}

				// Enable interpolation
				cmdWord = _moveActive ? 0x1F : 0x0F;

				// Count the realtime cycles
				++_CycleIdx;
				_vel += _acc;
				_pos += _vel;

				break;
			}

			// Not Operation enabled
			if (statusWord != 0x37) {
				_vel = 0.0;
				_acc = 0.0;
				_pos = (double)txMap->actPos;	// Make sure command and actual position are matching if not moveing
				if (_isEnabled) {
					_enable = false;	// Reset enable in error -> avoid auto enable after a reset!
				}
				_doHome = false;		// Make sure moves can only be called on a already enabled axis
				_stop = false;
				_jog = false;
				_move = false;
				_moveAbs = false;
				_moveActive = false;
			}

		} // Else disable
		else {
			_pos = (double)txMap->actPos;	// Make sure command and actual position are matching if not moveing
			_isEnabled = false;
			cmdWord = 0;
		}

		// Include Reset
		if (_doReset) {
			cmdWord = cmdWord | BIT_MASK_DRIVE_RESET;
			_doReset = false;
		}

		// Include home
		if (_doHome) {
			if (_homeDone) {
				_CycleIdx = 0;	// Start Cycle counting at 0
				_homeDone = false;
			}
			_moveActive = false;
			_error = false;
			// First switch opmode to Home (6)
			rxMap->opMode = 6;
			if (txMap->opMode == 6) {
				// Check home is started
				if (txMap->statusWord & 0x0010) {
					// Check for done
					if ((txMap->statusWord & BIT_MASK_DRIVE_MAN1_AKD) && (_CycleIdx > 15)) {
						_pos = txMap->actPos;
						_homeDone = true;
						_internalPosOverflow = 0;
						_doHome = false;
					}
					// Check for error
					if ((txMap->statusWord & BIT_MASK_DRIVE_MAN2_AKD) && (_CycleIdx > 15)) {
						_pos = txMap->actPos;
						_error = true;
						_doHome = false;
					}
				}
				// Start homing (Bit 4)
				_moveActive = true;
			}
		}
		else {	// Set interpolated position mode as default
			if (txMap->opMode != 7) {
				rxMap->opMode = 7;
			}
		}

		// Read active mode
		_actMode = txMap->opMode;
		// Map output data
		rxMap->cmdPos = (int32_t)_pos;		// Interpolated Position 
		rxMap->digitalOutput = _digitalOutput;
		rxMap->controlWord = cmdWord;	// DS402.CONTROLWORD
	}
}


// Enable/Disable procedure
void AkdInterpolatedPosition::enableDrive(bool enable) {
	_enable = enable && !getDriveFault();
}

// Stop any motion in progress [rpm/s]
void AkdInterpolatedPosition::setStop(double moveDec) {
	_cmdDec = (moveDec * POS_SCALE_DRIVE_AKD / 60.0 * CYCLE_TIME_IN_MS * CYCLE_TIME_IN_MS);	// Deceleration per cycle form [rpm/s]
	_stop = true;
}

// Reset drive faults
void AkdInterpolatedPosition::resetFault() {
	_doReset = true;
}

// Home axis
void AkdInterpolatedPosition::homeAxis() {
	_doHome = true;
}

// Move Relative [UU, rpm, rpm/s, rpm/s]
void AkdInterpolatedPosition::moveRelUU(double moveDistance, double moveVel, double moveAcc, double moveDec) {
	_cmdPos = (moveDistance / _posScaling * POS_SCALE_DRIVE_AKD);	// Position in increments from [UU]
	_cmdVel = (moveVel * POS_SCALE_DRIVE_AKD / 60.0 * CYCLE_TIME_IN_MS);		// Velocity per cycle from [rpm]
	_cmdAcc = (moveAcc * POS_SCALE_DRIVE_AKD / 60.0 * CYCLE_TIME_IN_MS * CYCLE_TIME_IN_MS);	// Acceleration per cycle form [rpm/s]
	_cmdDec = (moveDec * POS_SCALE_DRIVE_AKD / 60.0 * CYCLE_TIME_IN_MS * CYCLE_TIME_IN_MS);	// Deceleration per cycle form [rpm/s]
	_move = true;
}

// Move absolute [UU, rpm, rpm/s, rpm/s]
// ONLY ABSOLUTE FOR 32 BIT POSITION RESOLUTION
void AkdInterpolatedPosition::moveAbsUU(double moveDistance, double moveVel, double moveAcc, double moveDec) {
	_cmdPos = (moveDistance / _posScaling * POS_SCALE_DRIVE_AKD);	// Position in increments from [UU]
	_cmdVel = (moveVel * POS_SCALE_DRIVE_AKD / 60.0 * CYCLE_TIME_IN_MS);		// Velocity per cycle from [rpm]
	_cmdAcc = (moveAcc * POS_SCALE_DRIVE_AKD / 60.0 * CYCLE_TIME_IN_MS * CYCLE_TIME_IN_MS);	// Acceleration per cycle form [rpm/s]
	_cmdDec = (moveDec * POS_SCALE_DRIVE_AKD / 60.0 * CYCLE_TIME_IN_MS * CYCLE_TIME_IN_MS);	// Deceleration per cycle form [rpm/s]
	_moveAbs = true;
}

// Move velocity [rpm, rpm/s]
void AkdInterpolatedPosition::moveVel(double moveVel, double moveAcc) {
	_cmdVel = (moveVel * POS_SCALE_DRIVE_AKD / 60.0 * CYCLE_TIME_IN_MS);		// Velocity per cycle from [rpm]
	_cmdAcc = (moveAcc * POS_SCALE_DRIVE_AKD / 60.0 * CYCLE_TIME_IN_MS * CYCLE_TIME_IN_MS);	// Acceleration per cycle form [rpm/s]
	_jog = true;
}

// Set digital output (outIdx 1 or 2)
void AkdInterpolatedPosition::setDigOut(int outIdx, bool value) {
	uint32_t temp = (uint32_t)true << (outIdx + 15);
	if (value) {
		_digitalOutput |= temp;	// Set output true
	}
	else {
		_digitalOutput &= ~temp;	// Set output false
	}
}


// SDO control to the drive
uint64_t AkdInterpolatedPosition::sendSDOCmd(PSTATION_INFO pStation, bool write, USHORT idx, UCHAR subIdx, ULONG length, PUCHAR data) {
	if (write) {
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, idx, subIdx, length, data)) return -1;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, idx, subIdx, length, data)) return -2;
		return 1;
	}
	else {
		ULONG responesLen = 0;
		uint8_t responseData[MAX_ECAT_DATA] = { 0 };
		uint64_t outData2 = 0;
		if (ERROR_SUCCESS != Ecat64SdoInitUploadReq(pStation, idx, subIdx)) return -1;
		if (ERROR_SUCCESS != Ecat64SdoInitUploadResp(pStation, &responesLen, responseData)) return -2;
		// Byte add the result
		for (size_t i = 0; i < responesLen; i++) {
			outData2 += (uint64_t)responseData[i] << (i * 8);
		}
		// Get the correct sign for 32-Bit variables
		if (responesLen == 4) {
			outData2 = (uint64_t)outData2;
		}
		return outData2;
	}
}


// SDO download request
UINT64 AkdInterpolatedPosition::sdoControl_AKD(PSTATION_INFO pStation) {
	uint32_t _sdoData = 0;
	UINT64 _sdoWriteNo = 0;

	switch (_sdoWriteNo)
	{
	case 0:
		_sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 1:
		_sdoData = 0x00001a00;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x01, 2, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 2:
		_sdoData = 0x00001a01;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x02, 2, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 3:
		_sdoData = 0x00001a02;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x03, 2, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 4:
		_sdoData = 0x00000003;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 5:
		_sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 6:
		_sdoData = 0x60410010;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x01, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 7:
		_sdoData = 0x60770010;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x02, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 8:
		_sdoData = 0x60fd0020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x03, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 9:
		_sdoData = 0x00000003;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 10:
		_sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a01, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 11:
		_sdoData = 0x60630020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a01, 0x01, 4, (PUCHAR)&_sdoData))break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 12:
		_sdoData = 0x606c0020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a01, 0x02, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 13:
		_sdoData = 0x00000002;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a01, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 14:
		_sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a02, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 15:
		_sdoData = 0x10260208;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a02, 0x01, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 16:
		_sdoData = 0x60610008;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a02, 0x02, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 17:
		_sdoData = 0x00000002;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a02, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 18:
		_sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 19:
		_sdoData = 0x00001600;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x01, 2, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 20:
		_sdoData = 0x00001601;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x02, 2, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 21:
		_sdoData = 0x00001602;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x03, 2, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 22:
		_sdoData = 0x00000003;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 23:
		_sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 24:
		_sdoData = 0x10260108;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x01, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 25:
		_sdoData = 0x60400010;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x02, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 26:
		_sdoData = 0x60600008;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x03, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 27:
		_sdoData = 0x00000003;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 28:
		_sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1601, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 29:
		_sdoData = 0x60c10120;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1601, 0x01, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 30:
		_sdoData = 0x00000001;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1601, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 31:
		_sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1602, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 32:
		_sdoData = 0x60fe0120;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1602, 0x01, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 33:
		_sdoData = 0x00000001;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1602, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 34:
		_sdoData = 0x00000007;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x6060, 0x00, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 35:
		_sdoData = 0x00000002;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x60c2, 0x01, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 36:
		_sdoData = 0x000000fd;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x60c2, 0x02, 1, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 37:
		_sdoData = 0x00030000;		// BitMask for Digital Output (Bit 16+17)
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x60fe, 0x02, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 38:
		_sdoData = 0x00000010;		// FBUS.PARAM05 = 16 (To use costume scaling)
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x36e9, 0x00, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 39:
		_sdoData = (ULONG)POS_SCALE_DRIVE_AKD;//0x00100000;		// DS402.POSGEARSHAFTREV 1048576 (Set costume scaling)
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x6091, 0x02, 4, (PUCHAR)&_sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		_sdoWriteNo++;
	case 40:
		//*** Disable SDO automation ***
		pStation->SdoNum = 0;
		return -1;
	}

	//*** Something failed ***
	return _sdoWriteNo;
}
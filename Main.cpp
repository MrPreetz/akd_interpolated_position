//***************************************
//  Automated generated code template ***
//  EtherCAT Code Type : [Expert DC]  ***
//***************************************

// *****************************************************************
// Main Routine, splitted into Realtime- and Non- Realtime tasks
// 
// DISCLAIMER:
// [MIT](https://choosealicense.com/licenses/mit/)
// Copyright (c) [2021] [Oxni GmbH]
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Version	Date		Who			What
// *****************************************************************
// V 0.001	13.07.21	MRupf Oxni	First version
// *****************************************************************

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "c:\sha\globdef64.h"
#include "c:\sha\sha64debug.h"
#include "c:\eth\Sha64EthCore.h"
#include "c:\eth\Eth64CoreDef.h"
#include "c:\eth\Eth64Macros.h"
#include "c:\ect\Sha64EcatCore.h"
#include "c:\ect\Ecat64CoreDef.h"
#include "c:\ect\Ecat64Macros.h"
#include "c:\ect\Ecat64Control.h"
#include "c:\ect\Ecat64SilentDef.h"

//Configuration Header Includes
#include "DEFINES.h"
#include "AKD_IP.h"

#pragma comment( lib, "c:\\sha\\sha64dll.lib" )
#pragma comment( lib, "c:\\eth\\sha64ethcore.lib" )
#pragma comment( lib, "c:\\ect\\sha64ecatcore.lib" )

#pragma warning(disable:4996)
#pragma warning(disable:4748)


//#define REALTIME_PERIOD	   2000		//Realtime sampling period [usec]
//#define SYNC_CYCLES           1

//************************************
//#define SEQ_DEBUG			1
//************************************


//Declare global elements
PETH_STACK		__pUserStack = NULL;		//Ethernet Core Stack (used outside Realtime Task)
PETH_STACK		__pSystemStack = NULL;		//Ethernet Core Stack (used inside Realtime Task)
PSTATION_INFO	__pUserList = NULL;			//Station List (used outside Realtime Task)
PSTATION_INFO	__pSystemList = NULL;		//Station List (used inside Realtime Task)
USHORT			__StationNum = 0;			//Number of Stations
FP_ECAT_ENTER	__fpEcatEnter = NULL;		//Function pointer to Wrapper EcatEnter
FP_ECAT_EXIT	__fpEcatExit = NULL;		//Function pointer to Wrapper EcatExit
ULONG			__EcatState = 0;			//Initial Wrapper State
ULONG			__UpdateCnt = 0;			//Station Update Counter
ULONG			__LoopCnt = 0;				//Realtime Cycle Counter
ULONG			__ReadyCnt = 0;				//Ready state counter
STATE_OBJECT    __StateObject = { 0 };		//Cyclic state object
BOOLEAN			__bLogicReady = FALSE;		//Logic Ready Flag

// Some variables to play with
int64_t g_sdoData = 0;
AkdInterpolatedPosition g_Axis3(360.0, 3.0);	// One AKD Axis with 360� User Units per revolution and 3 Arms drive
int32_t g_DriveCmd;						// all drive commands
uint64_t currentTime = 0;
double g_MovePos = 90.0;		// [�]
double g_MoveVel = 600.0; 		// [rpm]
double g_MoveAcc = 1000.0;		// [rpm/s]
double g_MoveDec = 6000.0;		// [rpm/s]
bool g_DigOutReq = true;

//***************************
#ifdef SEQ_DEBUG
	SEQ_INIT_ELEMENTS(); //Init sequencing elements
#endif
//***************************


__inline PVOID __GetMapDataByName(char* szName, int Index, bool bInput)
{
	int cnt = 0;

	//Loop through station list
	for (USHORT i = 0; i < __StationNum; i++)
		if (strstr(__pUserList[i].szName, szName))
			if (cnt++ == Index)
			{
				//Get station pointer and return index
				if (bInput) { return __pUserList[i].RxTel.s.data; }
				else { return __pUserList[i].TxTel.s.data; }
			}

	//Station not found
	return NULL;
}


void static DoLogic()
{
	// Get run time of software in [ms]
	currentTime += (REALTIME_PERIOD * SYNC_CYCLES) / 1000;

	//Get mapped data pointers
	g_Axis3.realTimeTask((TX_MAP_AKD_v2*)__GetMapDataByName((char*)"AKD", 0, TRUE),    //1. Device AKD
		(RX_MAP_AKD_v2*)__GetMapDataByName((char*)"AKD", 0, false));   //1. Device AKD
	//ToDo: Payload logic
	//...

//****************************
#ifdef SEQ_DEBUG
	SYSTEM_SEQ_CAPTURE("SEQ", __LINE__, 0, TRUE);
#endif
//****************************
}


void static AppTask(void* param)
{
	//Call enter wrapper function
	__EcatState = __fpEcatEnter(
							__pSystemStack,
							__pSystemList,
							(USHORT)__StationNum,
							&__StateObject);


	//Check operation state and increase ready count
	if (__EcatState == ECAT_STATE_READY) { __ReadyCnt--; }
	else                                 { __ReadyCnt = SYNC_CYCLES; }

	//Check ready count
	if (__ReadyCnt == 1)
	{
		//**********************************
		if (__bLogicReady) { 
			DoLogic(); 
		}
		//**********************************

		//Update counter
		__UpdateCnt++;
	}

	//Call exit function
	__fpEcatExit();
	
	//Increase loop count
	__LoopCnt++;
}

void doRun() {


	// *** Execute commands form HMI ***
	// Bit 0 = enable
	if (g_DriveCmd & 1) {
		g_Axis3.enableDrive(!g_Axis3.getEnableStatus());
		// Reset bit command
		g_DriveCmd = g_DriveCmd & 0xFFFE;
	}
	// Bit 1 = stop
	if (g_DriveCmd & 2 | ((JOG_TIMEOUT_COUNT != 0) & (JOG_TIMEOUT_COUNT < currentTime))) {
		g_Axis3.setStop(g_MoveDec);
		// Reset bit command
		g_DriveCmd = g_DriveCmd & 0xFFFD;
		currentTime = 0;
	}
	// Bit 2 = reset fault
	if (g_DriveCmd & 4) {
		g_Axis3.resetFault();
		// Reset bit command
		g_DriveCmd = g_DriveCmd & 0xFFFB;
	}
	// Bit 3 = Jog
	if (g_DriveCmd & 8) {
		// Set a finit move time if configured
		if (JOG_TIMEOUT_COUNT != 0) currentTime = 0;
		g_Axis3.moveVel(g_MoveVel, g_MoveAcc);
		g_DriveCmd = g_DriveCmd & 0xFFF7;
	}
	// Bit 4 = Move rel
	if (g_DriveCmd & 16) {
		g_Axis3.moveRelUU(g_MovePos, g_MoveVel, g_MoveAcc, g_MoveDec);
		g_DriveCmd = g_DriveCmd & 0xFFEF;
	}
	// Bit 5 = home axis
	if (g_DriveCmd & 32) {
		g_Axis3.homeAxis();
		// Reset bit command
		g_DriveCmd = g_DriveCmd & 0xFFDF;
	}
	// Bit 6 = Write Digital output
	if (g_DriveCmd & 64) {
		g_Axis3.setDigOut(1, g_DigOutReq);
		g_DigOutReq != g_DigOutReq;	// toggle digital output with each call
	}
	// Bit 7 = Move abs
	if (g_DriveCmd & 128) {
		g_Axis3.moveAbsUU(g_MovePos, g_MoveVel, g_MoveAcc, g_MoveDec);
		g_DriveCmd = g_DriveCmd & 0xFF7F;
	}
	// Bit 10 = write sdo
	if (g_DriveCmd & 1024) {
		g_sdoData = 1000;
		g_Axis3.sendSDOCmd(&__pUserList[0], true, 0x356e, 0, 4, (PUCHAR)&g_sdoData);
		g_sdoData = -1000;
		g_Axis3.sendSDOCmd(&__pUserList[0], true, 0x356f, 0, 4, (PUCHAR)&g_sdoData);
		// Reset bit command
		g_DriveCmd = g_DriveCmd & 0xFBFF;
	}
	// Bit 11 = read sdo
	if (g_DriveCmd & 2048) {
		g_sdoData = g_Axis3.sendSDOCmd(&__pUserList[0], false, 0x356e, 0, 4, (PUCHAR)&g_sdoData);
		// Reset bit command
		g_DriveCmd = g_DriveCmd & 0xF7FF;
	}


	//Display TX and RX information
	//printf("Update Count: %i\r", __UpdateCnt);
	if (g_Axis3.getDriveFault() | g_Axis3.getDriveFault()) {
		printf("Drive Fault\r");
	}
	else {
		printf("Actual Pos: %.3f\r", g_Axis3.getActPosUU());
	}
}

// Service Data exchange
__inline ULONG __SdoControl(void)
{
	
	ULONG _status = 0;

	//Loop through station list
	for (USHORT i=0; i<__StationNum;i++)
	{
		 PSTATION_INFO pStation = (PSTATION_INFO)&__pUserList[i];
		 
		 //Do Device SDO control
		if (strstr(pStation->szName, "AKD")) {
			if (g_Axis3.sdoControl_AKD(pStation) == -1) {
				++_status;
			}
		}
	}
	// Both stations init ok
	if (_status == __StationNum) {
		return 0;
	}

	//Do default PDO assignment
	return Ecat64PdoAssignment();
	//return 0;
}

// Main routine
int main(void)
{
//******************
#ifdef SEQ_DEBUG
	SEQ_ATTACH();						//Attach to sequence memory
	SEQ_RESET(TRUE, FALSE, NULL, 0);	//Reset/Init sequence memory
#endif
//******************

	printf("\n*** EtherCAT Code Type: [Expert DC] ***\n\n");

	//Set ethernet mode
	__EcatSetEthernetMode(0, REALTIME_PERIOD, SYNC_CYCLES, TRUE);

	//Required ECAT parameters
	ECAT_PARAMS EcatParams;
	memset(&EcatParams, 0, sizeof(ECAT_PARAMS));
	EcatParams.PhysAddr = DEFAULT_PHYSICAL_ADDRESS;
	EcatParams.LogicalAddr = DEFAULT_LOGICAL_ADDRESS;
	EcatParams.SyncCycles = SYNC_CYCLES;			//Set cycles for synchronisation interval
	EcatParams.EthParams.dev_num = 0;				//Set NIC index [0..7]
	EcatParams.EthParams.period = REALTIME_PERIOD;	//Set realtime period [�sec]
	EcatParams.EthParams.fpAppTask = AppTask;

	//******************************
	//Create ECAT realtime core
	//******************************
	if (ERROR_SUCCESS == Sha64EcatCreate(&EcatParams))
	{
		Sleep(1000);

		//Init global elements
		__pUserStack	= EcatParams.EthParams.pUserStack;
		__pSystemStack	= EcatParams.EthParams.pSystemStack;
		__pUserList		= EcatParams.pUserList;
		__pSystemList	= EcatParams.pSystemList;
		__StationNum	= EcatParams.StationNum;
		__fpEcatEnter	= EcatParams.fpEcatEnter;
		__fpEcatExit	= EcatParams.fpEcatExit;

		//Display version information
		Sha64EcatGetVersion(&EcatParams);
		printf("ECTCORE-DLL : %.2f\nECTCORE-DRV : %.2f\n", EcatParams.core_dll_ver           / (double)100, EcatParams.core_drv_ver / (double)100);
		printf("ETHCORE-DLL : %.2f\nETHCORE-DRV : %.2f\n", EcatParams.EthParams.core_dll_ver / (double)100, EcatParams.EthParams.core_drv_ver / (double)100);
		printf("SHA-LIB     : %.2f\nSHA-DRV     : %.2f\n", EcatParams.EthParams.sha_lib_ver  / (double)100, EcatParams.EthParams.sha_drv_ver / (double)100);
		printf("\n");

		//Display station information
		for (int i = 0; i < __StationNum; i++) {
			printf("Station: %d\n", (ULONG)i);
			printf("%s\n", __pUserList[i].szName);

		}

		//******************************
		//Enable Stations
		//******************************

		//Change state to INIT
		if (ERROR_SUCCESS == Ecat64ChangeAllStates(AL_STATE_INIT))
		{
			UCHAR Data[0x100] = { 0 };

			//Reset devices
			Ecat64ResetDevices();
			Ecat64SendCommand(BWR_CMD, 0x0000,  0x300, 8, Data);

			//Set fixed station addresses and
			//Init FMMUs and SYNCMANs
			if (ERROR_SUCCESS == Ecat64InitStationAddresses(EcatParams.PhysAddr))
			if (ERROR_SUCCESS == Ecat64InitFmmus(EcatParams.LogicalAddr))
			if (ERROR_SUCCESS == Ecat64InitSyncManagers())
			{
				//Change state to PRE OPERATIONAL
				//Init PDO assignment
				if (ERROR_SUCCESS == Ecat64ChangeAllStates(AL_STATE_PRE_OP))
				if (ERROR_SUCCESS == __SdoControl())
				{
					//Drift compensation delay [msec]
					ULONG CompLoops = 1000;

					//Init DC immediately after cyclic operation has started
					//and get static master drift per msec (nsec unit)
					if (ERROR_SUCCESS == Ecat64ReadDcLocalTime())
					if (ERROR_SUCCESS == Ecat64CompDcOffset())
					if (ERROR_SUCCESS == Ecat64CompDcPropDelay())
					if (ERROR_SUCCESS == Ecat64CompDcDrift(&CompLoops))
					if (ERROR_SUCCESS == Ecat64DcControl())
					{
						//Init process telegrams (required for AL_STATE_SAFE_OP)
						Ecat64InitProcessTelegrams();

						//********************************************************
						//Start cyclic operation
						//********************************************************
						if (ERROR_SUCCESS == Ecat64CyclicStateControl(&__StateObject, AL_STATE_SAFE_OP)) { Sleep(500);
						if (ERROR_SUCCESS == Ecat64CyclicStateControl(&__StateObject, AL_STATE_OP))      { Sleep(100);

								//Set Logic Ready Flag
								__bLogicReady = TRUE;

								//Do a check loop
								printf("\nPress any key stop EtherCat ...\n");
								printf("Stations Number %d\n", __StationNum);


								// Run loop
								while (!kbhit()) {
									doRun();

									//Do some delay
									Sleep(100);
								}

								//Reset Logic Ready Flag
								__bLogicReady = FALSE;
								Sleep(100);
		
								//********************************************************
								//Stop cyclic operation
								//********************************************************
								Ecat64CyclicStateControl(&__StateObject, AL_STATE_INIT);
							}
						}
					}
				}
			}
		}
		//Destroy ECAT core
		Sha64EcatDestroy(&EcatParams);
	}

	//Check Status
	if		(EcatParams.EthParams.pUserStack == NULL)	{ printf("\n*** Ethernet Stack Failed ***\n"); }
	else if (EcatParams.EthParams.err_cnts.Phy != 0)	{ printf("\n*** No Link ***\n"); }
	else if (EcatParams.pUserList == NULL)				{ printf("\n*** No Station List ***\n"); }
	else if (EcatParams.StationNum == 0)				{ printf("\n*** No Station Found ***\n"); }
	else												{ printf("\n*** OK ***\n"); }

	//Wait for key press
	printf("\nPress any key ...\n"); 
	while (!kbhit()) { Sleep(100); }

//******************
#ifdef SEQ_DEBUG
	SEQ_DETACH(); //Detach from sequence memory
#endif
//******************
}

// ****************************************************************
// Kollmorgen AKD(c) Drive interface with motion profile calculation on the master (DS402 Mode of Operation 7)
//
// DISCLAIMER:
// [MIT](https://choosealicense.com/licenses/mit/)
// Copyright(c)[2021][Oxni GmbH]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this softwareand associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and /or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
// 
// The above copyright noticeand this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Version	Date		Who			What
// ****************************************************************
// V 0.001	06.10.21	MRupf Oxni	First version
// ****************************************************************
#pragma once

//*** Required header files ***
#include <windows.h>
#include <stdint.h>
#include "c:\sha\globdef64.h"
#include "c:\sha\Sha64Debug.h"
#include "c:\eth\Sha64EthCore.h"
#include "c:\eth\Eth64CoreDef.h"
#include "c:\eth\Eth64Macros.h"
#include "c:\ect\Sha64EcatCore.h"
#include "c:\ect\Ecat64CoreDef.h"
#include "c:\ect\Ecat64Macros.h"
#include "c:\ect\Ecat64Control.h"
#include "c:\ect\Ecat64SilentDef.h"
#include "Trapez_ProfilGen.h"

//*** Structures need to have 1 byte alignment ***
#pragma pack(push, old_alignment)
#pragma pack(1)

//*** TX mapping structure [Device Output] ***
struct TX_MAP_AKD_v2 {
	uint16_t statusWord;	// h1a00 h6041 2Byte	status word
	int16_t actCurrent;		// h1a00 h6077 2Byte	Actual current
	uint32_t digitalInput;	// h1a00 h60fd 4Byte	Digital input
	int32_t actPos;			// h1a01 h6063 4Byte	Actual Position
	int32_t actVel;			// h1a01 h606c 4Byte	Actual velocity
	uint8_t  dummy;			// h1a02 h1026 1Byte	Filler
	uint8_t  opMode;		// h1a02 h6061 1Byte	Actual mode of operation
};

//*** RX mapping structure [Device Input]  ***
struct RX_MAP_AKD_v2 {
	uint8_t dummy;			// h1600 h1026 1Byte	filler
	uint16_t controlWord;	// h1600 h6040 2Byte	contorl word
	uint8_t opMode;			// h1600 h6060 1Byte	mode of operation
	int32_t cmdPos;			// h1601 h60c1 4Byte	target position
	uint32_t digitalOutput;	// h1602 h60fe 4Byte	digital output
};

//Set back old alignment
#pragma pack(pop, old_alignment)

// AKD Class
class AkdInterpolatedPosition {
private:
	bool _enable = false;		// Enable/Disable Drive
	bool _isEnabled = false;
	bool _doReset = false;		// Enable routine will include reset in next call
	bool _doHome = false;		// Enable routine will control homing procdure
	bool _homeDone = false;		// Mirror of Home Done bit in Opmode Home
	bool _error = false;		// If any action fails
	bool _stop = false;			// stop movement
	bool _move = false;			// Enables interpolation of realitve move
	bool _moveAbs = false;		// Enables interpolation of absolute move
	bool _jog = false;			// Enables interpolation of velocity move
	bool _moveActive = false;	// Enables interpolation during moves
	double _cmdPos = 0;
	double _cmdPos2 = 0;
	double _cmdPos3 = 0;
	double _cmdVel = 0;
	int32_t _actVel = 0;
	double _cmdAcc = 0;
	double _cmdDec = 0;
	uint8_t _actMode = 0;
	double _posScaling;				// User Units per revolution
	float _driveIcont;				// Nominal current of the drive to scale actual current
	uint32_t _digitalOutput = 0;	// output are set zero by default
	int64_t _internalPos = 0;		// Internal position upscaled to 64 bit from 32 bit EtherCat
	int64_t _internalPosOverflow = 0;	// Internal position overflow counter
	const TX_MAP_AKD_v2* txMap;
	RX_MAP_AKD_v2* rxMap;

	Trapez_ProfilGen _profilGen;
	int32_t _CycleIdx = 0;
	int32_t _ProfilIdx = 0;
	double _pos = 0.0;
	double _vel = 0.0;
	double _acc = 0.0;

public:
	// SW Version
	static const uint32_t SW_VERSION = 0x1000;

	// Constructor
	AkdInterpolatedPosition(double iPosScaling, float iDriveCurr);

	// Return drive fault
	bool getDriveFault();

	// Return drive warning
	bool getDriveWarning();

	// Return home status
	bool getHomeStatus();

	// Return error status
	bool getErrorStatus();

	// Return enable status
	bool getEnableStatus();

	// Return status as bit combined int
	uint32_t getDriveStatus();

	// Get Actual Position as float in User Units
	double getActPosUU();

	// Get Actual Speed in RPM
	double getActVel();

	// Get Actual Current in Arms
	double getActCurrent();

	// Get digital input and output status
	uint32_t getDigInStat();


	// REALTIME TASK this routine has to be called in each cycle
	// Mapping of all commands to and from the EtherCat
	void realTimeTask(const TX_MAP_AKD_v2* txMap, RX_MAP_AKD_v2* rxMap);


	// Enable/Disable procedure
	void enableDrive(bool);

	// Stop any motion in progress [rpm/s]
	void setStop(double);

	// Reset drive faults
	void resetFault();

	// Home axis
	void homeAxis();

	// Move Relative [UU, rpm, rpm/s, rpm/s]
	void moveRelUU(double moveDistance, double moveVel, double moveAcc, double moveDec);

	// Move absolute [UU, rpm, rpm/s, rpm/s]
	// ONLY ABSOLUTE FOR 32 BIT POSITION RESOLUTION
	void moveAbsUU(double movePos, double moveVel, double moveAcc, double moveDec);

	// Move velocity [rpm, rpm/s]
	void moveVel(double moveVel, double moveAcc);

	// Set digital output  (outIdx 1 or 2 are only valid)
	void setDigOut(int outIdx, bool value);

	// SDO control to the drive
	static uint64_t sendSDOCmd(PSTATION_INFO pStation, bool iWrite, USHORT idx, UCHAR subIdx, ULONG length, PUCHAR data);

	// SDO download request
	static uint64_t sdoControl_AKD(PSTATION_INFO);

};


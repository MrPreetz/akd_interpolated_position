// ****************************************************************
// Trapezodial profile generator with acceleration, deceleration, move velocity and move position
// To generate an interpolated position with a jerk table (DS402 Mode of Operation 7/8)
//
// DISCLAIMER:
// [MIT](https://choosealicense.com/licenses/mit/)
// Copyright (c) [2021] [Oxni GmbH]
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Version	Date		Who			What
// ****************************************************************
// V 0.001	04.10.21	MRupf Oxni	First version
// ****************************************************************

#pragma once

//*** Required header files ***
#include <windows.h>
#include <stdint.h>


// Jerk table entrie
struct JerkTable {
	int32_t idx;		// index of the jerk value
	double jerk;		// Jerk value
};

//Set back old alignment
#pragma pack(pop, old_alignment)

// 
class Trapez_ProfilGen {
private:
	uint16_t _profilType = 0;		// No profile calculated

	// Convert cycle time to integer cycle numbers
	int32_t timeToCycleConv(double iCycleTime);

public:
	JerkTable _jerkTable[6] = { 0 };

	// Calculate position profile, units are increments (per cycle)
	// All inputs need to be positive!
	uint16_t calcMoveRel(double iActVel, double iPosCmd, double iVelCmd, double iAcc, double iDec);

	// Calculate acceleration profile, units are increments (per cycle)
	// All inputs need to be positive!
	uint16_t calcJog(double iActVel, double iVelCmd, double iAcc);
};


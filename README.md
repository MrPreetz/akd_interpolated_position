# README #

This repo introduces the Sybera EtherCat Master for motion with an Kollmorgen AKD drive in interpolated position
mode. In this mode the actual trajectory calculation is done in the master. 

The example code snippets covers the following functions:
  • Reading drive status
  • Reading actual position, velocity, current
  • Reading the digital inputs
  • Enable the drive
  • Reset drive faults
  • Executing the drive internal reference move
  • Execute trapeziodal motion (jog, move)
  • Setting the digital outputs
  • Read and write SDO data

The system uses a Spectra Power Box 100-J19 RTE and a AKD-P00306-NBCC.

### Sybera realtime Software ###

To use this example it is neccessary to have purchase a Sybera EtherCat Master Developer License. 

For any interest in the Sybera realtime software libraries, please contact anyone from the "Who do I talk too?"

### How do I get set up? ###

Use the MN_AKDInterpolatedPosition_010_En.pdf as manual.

The manual also exist in German language in the MN_AKDInterpolatedPosition_010_De.pdf. 

### Contribution guidelines ###

| Types | Naming convetion |
| ---- | ---- |
| constants | SCREAMING_SNAKE_CASE |
| struct, class | UpperCamelCase |
| function, methods, variables | lowerCamelCase |
| global variables | g_lowerCamelCase |
| member | _lowerCamelCase |

Respect the code, bring your ideas, raise issues and push code

Most important: HAVE FUN!

### Who do I talk to? ###

This repo is maintend by [oxni.ch](https://oxni.ch)

For any comment, question or inquiry about the implementation please contact [info@oxni.ch](mailto:info@oxni.ch)

For any comment, question or inquiry about the realtime software contact [info@sybera.de](mailto:info@sybera.de)


## License
The used Sybera library are under the Sybera software license agreement. 

The sybera libraries are not part of this publishing and need to be purchased seperatly.

[Sybera](https://www.sybera.de/download/software_lizenzierung.pdf)

For the rest.
[MIT](https://choosealicense.com/licenses/mit/)
Copyright (c) [2021] [Oxni GmbH]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
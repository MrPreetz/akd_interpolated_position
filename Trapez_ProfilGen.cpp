#include "Trapez_ProfilGen.h"
#include "DEFINES.h"
#include <cmath> 

// Convert cycle time to integer cycle numbers
int32_t Trapez_ProfilGen::timeToCycleConv(double iCycleTime)
{
    int32_t _cycles = (int32_t)iCycleTime;
    if (_cycles < 1) {    // convert negative numbers to positive cycles
        _cycles *= -1;
        if (_cycles < 1) {   //  Make sure acceleration has a minimum duration of 1 cycle
            _cycles = 1;
        }
    }
    return _cycles;
}

// Calculate profile, units are increments (per cycle)
// All inputs need to be positive, except iActVal!
uint16_t Trapez_ProfilGen::calcMoveRel(double iActVel, double iPosCmd, double iVelCmd, double iAcc, double iDec) {
    // empty jerk table
    for (size_t i = 0; i < 5; i++) {
        _jerkTable[i] = { 0 };
    }

    double _PosCmd = iPosCmd;
    double _Acc = 0.0;
    double _VelCmd = (_PosCmd > 0) ? abs(iVelCmd) * 1.0 : abs(iVelCmd) * -1.0;
    double _Dec = 0.0;
    double _Dec1 = 0.0;

    double _AccTime = abs(_VelCmd - iActVel) / iAcc;                           // Acceleration time
    double _DecTime = abs(_VelCmd) / iDec;                                             // Deceleration time
    double _DeltaDec = iActVel / iDec * iActVel / 2.0;                                       // Delta Position if stopped immeditaly
    double _DeltaAccDec = _AccTime * (_VelCmd + iActVel) / 2.0 + _DecTime * _VelCmd / 2.0;   // Delta position for acceleration and deceleration

    // Check if axis is not too fast
    if (_DeltaDec >= abs(iPosCmd)) {
        _DecTime = (iActVel) / iDec;
        int32_t _DecCycle = timeToCycleConv(_DecTime);
        _Dec = iActVel / (double)_DecCycle;   // get new acceleration value

        // Fill jerk table
        _jerkTable[0].idx = 0;
        _jerkTable[0].jerk = -_Dec;
        _jerkTable[1].idx = _DecCycle;
        _jerkTable[1].jerk = 0;

        _profilType = 1;    // Profile - "Already too fast" only brake with iDec to standstill
    }
    else if (abs(_DeltaAccDec) > abs(iPosCmd)) {
        // Calculate optimum peak velocity
        double _temp = (2.0 * iPosCmd * iAcc + iActVel * iActVel) / (1.0 + iAcc / iDec);
        if (_temp < 0) {
            _VelCmd = sqrt(-_temp) * -1.0;
        }
        else {
            _VelCmd = sqrt(_temp);
        }
        
        _AccTime = (_VelCmd - iActVel) / iAcc;
        int32_t _AccCycle = timeToCycleConv(_AccTime);
        _Acc = (_VelCmd - iActVel) / (double)_AccCycle;
        _DecTime = (_VelCmd) / iDec;
        int32_t _DecCycle = timeToCycleConv(_DecTime);
        //_Dec = iVelCmd / (double)_DecCycle;

        double _deltaPAcc = (iActVel + _VelCmd) / 2.0 * _AccCycle;
        double _firstDecV = (2.0 * (iPosCmd - _deltaPAcc) + iActVel - _VelCmd) / (1.0 + (double)_DecCycle); // *CYCLE_TIME_IN_MS;

        _Dec1 = _VelCmd - _firstDecV;
        _Dec = _firstDecV / (double)_DecCycle;

        // Fill jerk table
        _jerkTable[0].idx = 0;
        _jerkTable[0].jerk = _Acc;
        _jerkTable[1].idx = _AccCycle;
        _jerkTable[1].jerk = -_Dec1;
        _jerkTable[2].idx = _AccCycle + 1;
        _jerkTable[2].jerk = -_Dec;
        _jerkTable[3].idx = _AccCycle + 1 + _DecCycle;
        _jerkTable[3].jerk = 0;

        _profilType = 2;    // Profile - "Triangle move" iVelCmd will not be reached
    }
    else {
        int32_t _AccCycle = timeToCycleConv(_AccTime);
        _Acc = (_VelCmd - iActVel) / (double)_AccCycle;
        int32_t _DecCycle = timeToCycleConv(_DecTime);
        _Dec = _VelCmd / (double)_DecCycle;

        double _deltaPAcc = (iActVel + _VelCmd) / 2.0 * _AccCycle;
        double _deltaPDec = _VelCmd * _DecCycle / 2.0;
        double _ConstMoveCycle = (iPosCmd - _deltaPAcc - _deltaPDec) / _VelCmd;
        int32_t _ConstMoveCyleInt = timeToCycleConv(_ConstMoveCycle + 0.5);
        double _PError = (_ConstMoveCycle - _ConstMoveCyleInt )* _VelCmd;

        double _firstDecV = (2.0 * (_deltaPDec + _PError + iActVel / 2.0) - _VelCmd) / (1.0 + (double)_DecCycle);
        _Dec1 = _VelCmd - _firstDecV;
        _Dec = _firstDecV / (double)_DecCycle;

        // Fill jerk table
        _jerkTable[0].idx = 0;
        _jerkTable[0].jerk = _Acc;
        _jerkTable[1].idx = _AccCycle;
        _jerkTable[1].jerk = 0;
        _jerkTable[2].idx = _AccCycle + _ConstMoveCyleInt;
        _jerkTable[2].jerk = -_Dec1;
        _jerkTable[3].idx = _AccCycle + _ConstMoveCyleInt + 1;
        _jerkTable[3].jerk = -_Dec;
        _jerkTable[4].idx = _AccCycle + _ConstMoveCyleInt + 1 + _DecCycle;
        _jerkTable[4].jerk = 0;

        _profilType = 4;    // Profile - "Regular move"
    }

    return _profilType;
}


// Calculate acceleration profile, units are increments (per cycle)
// All inputs need to be positive!
uint16_t Trapez_ProfilGen::calcJog(double iActVel, double iVelCmd, double iAcc) {
    // empty jerk table
    for (size_t i = 0; i < 5; i++) {
        _jerkTable[i] = { 0 };
    }

    double _AccTime = (iVelCmd - iActVel) / iAcc;           // Acceleration time
    int32_t _AccCycle = timeToCycleConv(_AccTime);
    double _Acc = (iVelCmd - iActVel) / (double)_AccCycle;   // get new acceleration value;

    // Fill jerk table
    _jerkTable[0].idx = 0;
    _jerkTable[0].jerk = _Acc;
    _jerkTable[1].idx = _AccCycle;
    _jerkTable[1].jerk = 0;

    return _profilType = 8;    // Profile - "Jog" only change speed to new speed
}
